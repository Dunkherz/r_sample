# CODEBOOK:

## Where the data came from:
Data download from coursera assigment week 4
Human Activity Recognition Using Smartphones Dataset
https://d396qusza40orc.cloudfront.net/getdata%2Fprojectfiles%2FUCI%20HAR%20Dataset.zip

## variables:
- x_train, y_train, x_test, y_test, Sub_train, Sub_test, labels and features contain the data from the downloaded files.
- X, Y and Sub merge train and test data togeter.
- selected_mean_std contains the columns where mean and standard desviation is stored
- X_clean is a subset with only the mean and standard desviation
- clean_data_mean is a subset with the tidy data


## About run_analysis.R
File performs the 5 following steps (in accordance assigned task of course work):

1. Merges the training and the test sets to create one data set.
2. Extracts only the measurements on the mean and standard deviation for each measurement.
3. Uses descriptive activity names to name the activities in the data set
4. Appropriately labels the data set with descriptive variable names.
5. From the data set in step 4, creates a second, independent tidy data set with the average of each variable for each activity and each subject.
